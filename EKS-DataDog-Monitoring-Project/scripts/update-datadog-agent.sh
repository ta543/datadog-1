#!/bin/bash

echo "Updating DataDog agent DaemonSet..."
kubectl apply -f ../datadog/agents/datadog-agent-daemonset.yaml -n kube-system
echo "DataDog agent updated successfully."
