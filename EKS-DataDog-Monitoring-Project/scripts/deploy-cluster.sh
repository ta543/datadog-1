#!/bin/bash

echo "Deploying AWS EKS cluster..."
eksctl create cluster -f cluster-config.yaml
echo "Cluster deployment complete."
