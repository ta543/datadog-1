# 📊 AWS EKS Monitoring with DataDog 🐶

Welcome to the GitHub repository for **AWS EKS Monitoring with DataDog**! This project is designed to help you set up and monitor an AWS Elastic Kubernetes Service (EKS) cluster using DataDog. We provide detailed configurations for deploying a Kubernetes cluster, integrating DataDog for comprehensive monitoring, and establishing custom dashboards and alerts to offer valuable insights into your cluster's operations.

## 🗂 Project Structure

This repository is neatly organized into several directories to help you navigate and find files easily:

- `deployment/`: Contains Kubernetes manifests and Helm charts for deploying applications and services to the EKS cluster.
- `datadog/`: Holds configurations for DataDog agents, APM settings, dashboards, and monitoring alerts.
- `scripts/`: Features utility scripts for creating the EKS cluster, installing necessary tools, and updating DataDog agents.
- `logs/`: (Local directory) Stores logs from the application and system operations, which are useful for development and troubleshooting purposes.

## 🚀 Prerequisites

Before diving into the project, please make sure you have the following tools and accounts set up:

- **`kubectl`**: The Kubernetes command-line tool that allows you to run commands against Kubernetes clusters.
- **`eksctl`**: A simple CLI tool for creating clusters on AWS EKS.
- **AWS account**: Ensure you have an AWS account with configured access credentials.
- **DataDog account**: A DataDog account with an API key is required for monitoring setup.

# 📜 License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details. 📄
